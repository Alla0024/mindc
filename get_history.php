<?php
require_once("config/db.php");
$conn = dbConnect();

$sql = "SELECT * FROM users_info ORDER BY id DESC";
$result = $conn->query($sql);
$historyData = [];

if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $historyData[] = array(
            'id' => $row['id'],
            'username' => $row['username'],
            'userInput' => $row['users_input'],
            'fibonacciNumber' => $row['fibonacci_number'],
            'ipAddress' => $row['ip_address']
        );
    }
}

$response = array(
    'success' => true,
    'history' => $historyData
);

echo json_encode($response);
