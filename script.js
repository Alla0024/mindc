$(document).ready(function () {
    $('#fibonacciForm').submit(function (event) {
        event.preventDefault();

        var formData = {
            action: 'add',
            username: $('#username').val(),
            number: $('#number').val(),
        };

        $.ajax({
            url: 'index.php',
            method: 'POST',
            dataType: 'json',
            data: formData,
            success: function (response) {
                if (response.success) {
                    $('#fibonacciResult').text(response.fibonacciNumber);
                    $('#resultSection').show();

                    $('#username').val('');
                    $('#number').val('');

                    reloadHistoryTable();
                } else {
                    console.error('Error: ' + response.message);
                }
            },
            error: function (xhr, status, error) {
                console.error('AJAX Error: ', error);
            }
        });
    });

    loadHistoryData();

    function loadHistoryData() {
        $.ajax({
            url: 'get_history.php',
            method: 'GET',
            dataType: 'json',
            success: function (response) {
                if (response.success) {
                    var historyData = response.history;
                    populateHistoryTable(historyData);
                } else {
                    console.error('Error: ' + response.message);
                }
            },
            error: function (xhr, status, error) {
                console.error('AJAX Error: ', error);
            }
        });
    }

    function reloadHistoryTable() {
        $('#historyTable').DataTable().destroy();

        var table = $('#historyTable');
        table.DataTable().clear().draw();

        loadHistoryData();
    }

    function populateHistoryTable(historyData) {
        var table = $('#historyTable');

        if ($.fn.DataTable.isDataTable(table)) {
            table.DataTable().destroy();
        }

        table.DataTable({
            data: historyData,
            columns: [
                {data: 'username'},
                {data: 'userInput'},
                {data: 'fibonacciNumber'},
                {data: 'ipAddress'},
                {
                    data: null,
                    render: function (data, type, row) {
                        return '<button class="btn btn-sm btn-primary edit-btn" data-id="' + row.id + '">Edit</button>' +
                            '<button class="btn btn-sm btn-danger delete-btn" data-id="' + row.id + '">Delete</button>';
                    }
                }
            ],
            order: [[0, 'asc']],
            paging: true,
            searching: true,
            lengthChange: false,
            pageLength: 10,
            language: {
                emptyTable: 'No data available',
                info: 'Showing _START_ to _END_ of _TOTAL_ entries',
                infoEmpty: 'Showing 0 to 0 of 0 entries',
                infoFiltered: '(filtered from _MAX_ total entries)',
                zeroRecords: 'No matching records found',
                search: 'Search:',
                paginate: {
                    first: 'First',
                    last: 'Last',
                    next: 'Next',
                    previous: 'Previous'
                }
            }
        });
    }

    $('#historyTable').on('click', '.edit-btn', function () {
        var table = $('#historyTable').DataTable();
        var row = table.row($(this).parents('tr'));
        var data = row.data();

        $('#editUsername').val(data.username);
        $('#editNumber').val(data.userInput);
        $('#editFibonacciNumber').val(data.fibonacciNumber);
        $('#editIpAddress').val(data.ipAddress);
        $('#editId').val(data.id);

        $('#editModal').modal('show');
    });

    $('#saveChangesBtn').click(function () {
        var id = $('#editId').val();
        var username = $('#editUsername').val();
        var userInput = $('#editNumber').val();

        updateData(id, username, userInput);

        $('#editModal').modal('hide');
    });

    $('#historyTable').on('click', '.delete-btn', function () {
        var table = $('#historyTable').DataTable();
        var row = table.row($(this).parents('tr'));
        var data = row.data();

        var confirmDelete = confirm('Are you sure you want to delete this row?');

        if (confirmDelete) {
            row.remove().draw();

            deleteData(data.id);
        }
    });

    function deleteData(id) {
        $.ajax({
            url: 'index.php',
            method: 'POST',
            dataType: 'json',
            data: {
                action: 'delete',
                id: id
            },
            success: function (response) {
                if (response.success) {
                    console.log('Data deleted successfully');
                    reloadHistoryTable();
                } else {
                    console.error('Error deleting data: ' + response.message);
                }
            },
            error: function (xhr, status, error) {
                console.error('AJAX Error: ', error);
            }
        });
    }

    function updateData(id, username, updatedNumber) {
        $.ajax({
            url: 'index.php',
            method: 'POST',
            dataType: 'json',
            data: {
                action: 'update',
                id: id,
                updatedUsername: username,
                updatedNumber: updatedNumber
            },
            success: function (response) {
                if (response.success) {
                    console.log('Data updated successfully');
                    reloadHistoryTable();
                } else {
                    console.error('Error updating data: ' + response.message);
                }
            },
            error: function (xhr, status, error) {
                console.error('AJAX Error: ', error);
            }
        });
    }
});
