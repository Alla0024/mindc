<?php
require_once("config/db.php");
$conn = dbConnect();

$response = '';
$action = filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING);
if ($action === 'add') {
    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $number = filter_input(INPUT_POST, 'number', FILTER_VALIDATE_INT);
    $fibonacci_number = fibonacci($number);

    if ($username !== null && $username !== false && $number !== null && $number !== false && $fibonacci_number !== null && $fibonacci_number !== false) {
        addData($username, $number, $fibonacci_number, $conn);
        $response = array('success' => true, 'fibonacciNumber' => $fibonacci_number);
    } else {
        $response = array('success' => false, 'message' => 'Invalid input');
    }
} elseif ($action === 'update') {
    $id = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT);
    $updatedUsername = filter_input(INPUT_POST, 'updatedUsername', FILTER_SANITIZE_STRING);
    $updatedNumber = filter_input(INPUT_POST, 'updatedNumber', FILTER_VALIDATE_INT);
    $updatedFibonacciNumber = fibonacci($updatedNumber);

    if ($id !== null && $id !== false && $updatedUsername !== null && $updatedUsername !== false && $updatedNumber !== null && $updatedNumber !== false && $updatedFibonacciNumber !== null && $updatedFibonacciNumber !== false) {
        updateData($id, $updatedUsername, $updatedNumber, $updatedFibonacciNumber, $conn);
        $response = array('success' => true);
    } else {
        $response = array('success' => false, 'message' => 'Invalid input');
    }

} elseif ($action === 'delete') {
    $id = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT);

    if ($id !== null && $id !== false) {
        deleteData($id, $conn);
        $response = array('success' => true);
    } else {
        $response = array('success' => false, 'message' => 'Invalid input');
    }
}

echo json_encode($response);

function fibonacci($n)
{
    if ($n <= 1) {
        return $n;
    } else {
        return fibonacci($n - 1) + fibonacci($n - 2);
    }
}

function updateData($id, $username, $number, $fibonacciNumber, $conn)
{
    $stmt = $conn->prepare("UPDATE users_info SET username = ?, users_input = ?, fibonacci_number = ? WHERE id = ?");
    $stmt->bind_param("siii", $username, $number, $fibonacciNumber, $id);
    $stmt->execute();
    $stmt->close();
}


function deleteData($id, $conn)
{
    $stmt = $conn->prepare("DELETE FROM users_info WHERE id = ?");
    $stmt->bind_param("i", $id);
    $stmt->execute();
    $stmt->close();
}

function addData($username, $number, $fibonacci_number, $conn)
{
    $ipAddress = $_SERVER['REMOTE_ADDR'];
    $stmt = $conn->prepare("INSERT INTO users_info (username, users_input, fibonacci_number, ip_address) VALUES (?, ?, ?, ?)");
    $stmt->bind_param("siss", $username, $number, $fibonacci_number, $ipAddress);
    $stmt->execute();
    $stmt->close();
}
